package group_iterator;

/**
 * A class that aproximates the value of Pi with a Monte Carlo Algorithm
 * 
 * 
 * @author Giancarlo Bergamin
 *
 */

public class Pi {
	
	/**
	 * Private fields for the radius of the circle, for the stored value of pi, for the number
	 * of points inside the square around the circle and for the number of points inside this circle
	 */
	
	private int radius;
	private double pi;
	private int numberPoints;
	private int inside;
	
	/**
	 * Consturctor of the class: Sets the radius of the circle to 1, initalizes the variables
	 * pi and inside
	 * 
	 * @param numberPoints The number of points to be created for the aproximation of pi, more 
	 * 	points results in a better aproximation
	 */
	
	public Pi(int numberPoints){
		radius = 1;
		pi = 0;
		this.numberPoints = numberPoints;
		inside = 0;
	}
	
	/**
	 * Main method, initalizes a new Pi Object with 10000 points to be created inside of the square 
	 * around the circle. Prints the aproximation of the value for pi
	 * 
	 * @param args default parameter for the main method
	 */
	
	public static void main(String[] args){
		
		Pi pi = new Pi(10000);
		System.out.println(pi.aproximate());
		
	}
	
	/**
	 * The method that aproximates pi. It creates a certain number of points spezified by
	 * the constructor in a quarter of the square that is around the circle with the radius 1
	 * and checks if the point is inside the circle. If yes then increment a counter by 1, if no do nothing
	 * Then divide the number of points inside the circle by the number of total points created and 
	 * multiply it with 4. Thats the aproximated value for pi.
	 * 
	 * @return Returns a double value for pi
	 */
	
	public double aproximate(){
		int counter = numberPoints;
		
		while(counter > 0){
			double pointx = Math.random();
			double pointy = Math.random();
			
			if (pointx * pointx + pointy * pointy <= 1){
				inside++;
			}
			counter--;
		}
		pi = 4*(double)inside/numberPoints;
		
		return pi;
	}
	
}
