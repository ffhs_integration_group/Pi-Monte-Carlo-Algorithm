package group_iterator;
import static org.junit.Assert.*;
import org.junit.Test;

/**
 * A test class fot the Pi class
 * 
 * @author Giancarlo Bergamin
 *
 */

public class PiTest {

	
	/**
	 * Tests if the aproximation of pi is in a certain range around the constant
	 * Math.PI. The aproximation for pi is allowed to be 5% of.
	 * Create a new instance of the Pi class and test if it is in the allowed range.
	 */
	@Test
	public void testAccuracy() {
		double tolerance = 0.05 * Math.PI;
		Pi pi = new Pi(10000);
		
		assertEquals(pi.aproximate(),Math.PI, tolerance);
	}

}
